using System.Collections.Generic;
using UnityEngine;

namespace common
{
    public class ComputeShaderFinder
    {
        private readonly static Dictionary<string, ComputeShader> nameMap = new Dictionary<string, ComputeShader>();


        private static ComputeShader[] listAllComputeShader()
        {
            
            Object[] objects = Resources.LoadAll("");
            string folder = "/Users/crypt/projects/rgb/Unity/Shaders-examples/ComputeShaderExample/Assets/root-shaders";
            Resources.LoadAll(folder);
            return (ComputeShader[]) Resources
                .FindObjectsOfTypeAll(typeof(ComputeShader));
        }

        private static void initNameMap()
        {
            ComputeShader[] computeShaders = listAllComputeShader();
            foreach (ComputeShader computeShader in computeShaders)
            {
                nameMap.Add(computeShader.name, computeShader);
                Console.log("ComputeShader " + computeShader.name);
            }
        }

        public static ComputeShader find(string name)
        {
            if (nameMap.Count == 0)
            {
                initNameMap();
            }

            if (nameMap.ContainsKey(name))
            {
                return nameMap[name];
            }

            Console.log("Unable to find the Compute Shader with name: " + name);
            return null;
        }
    }
}