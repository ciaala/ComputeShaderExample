﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace common
{
    public class Console : MonoBehaviour
    {
        public enum LEVEL
        {
            DEBUG,
            LOG,
            ERROR
        }

        public static Console current = new Console();
        public static LEVEL level;
        public LEVEL _level;
        private Text uiText;

        public Console()
        {
        }

        public void Awake()
        {
            current.uiText = GetComponent<Text>();
        }

        public static void debug(String message)
        {
            if (level == LEVEL.DEBUG)
                current.write(LEVEL.DEBUG, message);
        }

        public static void log(String message)
        {
            if (level == LEVEL.DEBUG || level == LEVEL.LOG)
                current.write(LEVEL.LOG, message);
        }

        public static void err(String message)
        {
            current.write(LEVEL.ERROR, message);
        }

        private void write(LEVEL messageLevel, String message)
        {
            String levelSymbol = messageLevel == LEVEL.ERROR ? "E" : messageLevel == LEVEL.LOG ? "L" : "D";

            string textToLog = levelSymbol + ">" + message + "\n";
            if (uiText != null)
            {
                uiText.text += textToLog;
            }
            else
            {
                TextWriter textWriter;
                switch (messageLevel)
                {
                    case LEVEL.DEBUG:
                    case LEVEL.LOG:
                        textWriter = System.Console.Out;
                        break;
                    default:
                        textWriter = System.Console.Error;
                        break;
                        
                }
                textWriter.Write(textToLog);
            }
        }

        public LEVEL Level
        {
            get => _level;
            set
            {
                _level = value;
                level = value;
            }
        }
    }
}