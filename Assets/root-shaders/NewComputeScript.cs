﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewComputeScript : MonoBehaviour
{
    public ComputeShader shader;
    public int TextResolution = 256;
    Renderer renderer;
    RenderTexture myRenderTexture;
    public Vector2 snakePosition = new Vector2(2f,2f);

    // Start is called before the first frame update
    void Start()
    {
      myRenderTexture = new RenderTexture(TextResolution, TextResolution, 24);
      myRenderTexture.enableRandomWrite = true;
      myRenderTexture.Create();

      renderer =GetComponent<Renderer>();
      renderer.enabled = true;

      UpdateTextureFromCompute();
    }


    private void UpdateTextureFromCompute() {
      int kernelHandle = shader.FindKernel("CSMain");
      int RandOffset = (int) (Time.timeSinceLevelLoad*100);
    //  Debug.Log("RandOffset: " + RandOffset);
      shader.SetInt("RandOffset", RandOffset);
    //  shader.SetVector("snakePosition", snakePosition );
      //Debug.Log("snakePosition" + snakePosition.x + " "+  snakePosition.y);
      shader.SetTexture(kernelHandle, "Result", myRenderTexture);
      shader.Dispatch(kernelHandle, TextResolution/8, TextResolution/8, 1);
      renderer.material.SetTexture("_MainTex", myRenderTexture);
    }


    // Update is called once per frame
    void Update()
    {
      if (Input.GetKeyUp(KeyCode.Alpha4))
        UpdateTextureFromCompute();
    }





}
