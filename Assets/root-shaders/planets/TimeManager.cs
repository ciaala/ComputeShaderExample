﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection.Emit;
using planets;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public static TimeManager current;

    public double virtualTime;
    public String virtualTimeString;
    
    private readonly List<TimeUnit> timeUnits;

    public TimeManager()
    {
        const long oneSecond = 1000;
        const long oneMinute = 60 * oneSecond;
        const long oneHour = 60 * oneMinute;
        const long oneDay = 24 * oneHour;
        const long oneMonth = 30 * oneDay;
        const long oneYear = 365 * oneDay;
        
        timeUnits = new List<TimeUnit>
        {
            new TimeUnit("year", oneYear),
            new TimeUnit("month", oneMonth),
            new TimeUnit("day", oneDay),
            new TimeUnit("hour", oneHour),
            new TimeUnit("minute", oneMinute),
            new TimeUnit("minute", oneMinute),
            new TimeUnit("second", oneSecond)
        };
    }

    void Awake()
    {
        current = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        GameEvents.current.onTimeSliderChange += onTimeSliderChange;
        GameEvents.current.onVirtualTimeIncrement += onVirtualTimeIncrement;
    }

    private void onVirtualTimeIncrement(float increment)
    {
        virtualTime += increment * 1000;
        String text = timeToString((long) virtualTime);
        virtualTimeString = text;
        GameEvents.current.virtualTimeUpdate(text);
    }

    private void onTimeSliderChange(float value)
    {
        String text = timeToString((long) Math.Pow(10, value) * 1000);
        GameEvents.current.virtualTimeChange(text);
    }

    private readonly struct TimeUnit
    {
        private readonly string label;
        private readonly long unitInMillis;

        public TimeUnit(string label, long unitInMillis)
        {
            this.label = label;
            this.unitInMillis = unitInMillis;
        }

        public string getLabel()
        {
            return label;
        }

        public long getUnitInMillis()
        {
            return unitInMillis;
        }
    }
    

    private static string timeToString(long virtualTime)
    {
        string text = "";
        
        foreach (TimeUnit timeUnit in current.timeUnits)
        {
            timeUnitToString(ref virtualTime, ref text, timeUnit);
        }
        if (virtualTime > 0)
        {
            text += (int) virtualTime;
        }
        return text;
    }

    private static void timeUnitToString(ref long virtualTime, ref string text, TimeUnit timeUnit)
    {
        long unitInMillis = timeUnit.getUnitInMillis();
        if (virtualTime > unitInMillis)
        {
            long units = virtualTime / unitInMillis;
            text += units + " " + timeUnit.getLabel() + (units > 1 ? "s " : " ");
            virtualTime = virtualTime % unitInMillis;
        }
    }
}