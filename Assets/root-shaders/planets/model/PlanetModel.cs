using System;

namespace planets.model
{
    public struct PlanetModel
    {
        public readonly String name;
        public readonly double mass;
        public readonly double diameter;
        public readonly double orbitalPeriod;
        public readonly double orbitalInclination;
        public readonly double perihelion;
        public readonly double aphelion;
        public readonly double distanceFromSun;

        public PlanetModel(string name,
            double mass,
            double diameter,
            double distanceFromSun,
            double perihelion,
            double aphelion,
            double orbitalPeriod,
            double orbitalInclination)
        {
            this.name = name;
            this.mass = mass;
            this.diameter = diameter;
            this.orbitalPeriod = orbitalPeriod;
            this.orbitalInclination = orbitalInclination;
            this.perihelion = perihelion;
            this.aphelion = aphelion;
            this.distanceFromSun = distanceFromSun;
        }
    }
}