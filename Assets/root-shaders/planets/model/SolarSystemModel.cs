using planets.model;

public class SolarSystemModel
{
    internal PlanetModel[] planets;
    internal PlanetModel sun;
    public SolarSystemModel()
    {
        initPlanets();
    }
    public void initPlanets()
    {
        planets = new PlanetModel[9];
        sun = new PlanetModel("Sun", 1988500, 2 * 695700, 0, 0, 0, 0, 0);
        planets[0] = new PlanetModel("Mercury", 0.33, 4879, 57.9, 46, 69.8, 88, 7.0);
        planets[1] = new PlanetModel("Venus", 4.87, 12104, 108.2, 107.5, 108.9, 224.7, 3.4);
        planets[2] = new PlanetModel("Earth", 5.97, 12756, 149.6, 147.1, 152.1, 365.2, 0.0);
        planets[3] = new PlanetModel("Mars", 0.642, 6792, 227.9, 206.6, 249.2, 687.0, 1.9);
        planets[4] = new PlanetModel("Jupiter", 1898, 142984, 778.6, 740.5, 816.6, 4331, 1.3);

        planets[5] = new PlanetModel("Saturn", 568, 120536, 1433.5, 1352.6, 1514.5, 10747, 2.5);
        planets[6] = new PlanetModel("Uranus", 86.8, 51118, 2872.5, 2741.3, 3003.6, 30589, 0.8);
        planets[7] = new PlanetModel("Neptune", 102, 49528, 4495.1, 4444.5, 4545.7, 59800, 1.8);

        planets[8] = new PlanetModel("Pluto", 0.0146, 2370, 5906.4, 4436.8, 7375.9, 90560, 17.2);
        
    }

    public PlanetModel[] getPlanets()
    {
        return planets;
    }
}