namespace planets.compute
{
    public struct PlanetData
    {
        private static readonly int countFieldsInplanetsData = 9;
        public static readonly int StrideSize = sizeof(float) * countFieldsInplanetsData + sizeof(int);

        public float fDebug;
        public int iDebug;

        public float mass;
        public float diameter;
        public float distanceFromSun;

        public float orbitalPeriod;
        public float orbitalInclination;

        public float angularSpeed;

        // written too
        public float angularPosition;

        // milliSeconds 
        // 1000x1000
        public float microAngularPosition;


        public override string ToString()
        {
            return ""
                   + "ang: " + angularPosition
                   + ", m_ang: " + microAngularPosition
                   + ", ang_speed: " + angularSpeed
                   //            + ", mass: " + mass
                   //            + ", orbitalPeriod: " + orbitalPeriod
                   //             + ", orbitalInclination: " + orbitalInclination
                   //             + ", distanceFromTheSun: " + distanceFromSun
                   //             + ", diameter: " + diameter
                   + ", fDebug: " + fDebug
                //             + ", iDebug: " + iDebug
                ;
        }
    }
}