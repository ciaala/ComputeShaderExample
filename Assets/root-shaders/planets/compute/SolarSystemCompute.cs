﻿using System;
using System.Collections;
using JetBrains.Annotations;
using planets.model;
using UnityEditor;
using UnityEngine;
using Console = common.Console;

namespace planets.compute
{
    public class SolarSystemCompute
    {

        private readonly SolarSystemModel model;

        // 24*60*60
        private float SECONDS_IN_DAY = 86400;

        
        // public int SphereAmount = 17;


        private ComputeBuffer resultBuffer;
        private ComputeBuffer planetBuffer;
        int kernel;
        uint threadGroupSize;
        Vector3[] output;
        //private ArrayList tails = new ArrayList(10);

        private int planetsAmount;

        private double engineTimeCoefficient = 1;
        private PlanetData[] _planetDatas;

        
        private readonly GameEvents gameEvents;
        private readonly ComputeShader shader;

        // Start is called before the first frame update
        public SolarSystemCompute(SolarSystemModel model, GameEvents gameEvents, ComputeShader shader)
        {
            this.model = model;
            this.gameEvents = gameEvents;
            this.shader = shader;
            
            // Event System
            gameEvents.onTimeSliderChange += OnTimeSliderChange;

            // Model
            initSolarSystem();

            // Shader 
            initComputingShaders();
        }

        private void initComputingShaders()
        {
            kernel = shader.FindKernel("planets");
            Debug.Log("Kernel Index: " + kernel);
            if (kernel < 0)
            {
                Debug.LogError("Unable to find the shader");
            }
            output = new Vector3[model.planets.Length];
            //buffer on the gpu in the ram
            resultBuffer = new ComputeBuffer(planetsAmount, sizeof(float) * 3);
            shader.GetKernelThreadGroupSizes(kernel, out threadGroupSize, out _, out _);
        }

        private void OnTimeSliderChange(float timeCoefficient)
        {
            engineTimeCoefficient = Math.Pow(10, timeCoefficient);
            // engineTimeCoefficient = 1;
            //Debug.Log("Change engineTimeCoefficient " + engineTimeCoefficient);
        }

        public void initSolarSystem()
        {
            // Planet Sheet
            planetsAmount = model.planets.Length;

            // Planet Buffer

            planetBuffer = new ComputeBuffer(planetsAmount, PlanetData.StrideSize, ComputeBufferType.Structured);
            initPlanetsData();

        }
        
        private void initPlanetsData()
        {
            _planetDatas = new PlanetData[model.planets.Length];
            for (var index = 0; index < model.planets.Length; index++)
            {
                PlanetModel planetModel = model.planets[index];
                PlanetData planetData = new PlanetData();
                planetData.mass = (float) planetModel.mass;
                planetData.diameter = (float) planetModel.diameter;
                planetData.distanceFromSun = (float) planetModel.distanceFromSun;
                planetData.orbitalInclination = (float) planetModel.orbitalInclination;
                planetData.orbitalPeriod = (float) planetModel.orbitalPeriod;

                double orbitalPeriodSeconds = planetModel.orbitalPeriod * SECONDS_IN_DAY / 1000;
                planetData.angularSpeed = (float) (1000 * 2 * Math.PI / orbitalPeriodSeconds);

                Debug.Log(planetModel.name + " " + planetData.angularSpeed);
                planetData.angularPosition = 0;
                planetData.microAngularPosition = 0;
                _planetDatas[index] = planetData;
            }

            planetBuffer.SetData(_planetDatas);
        }
        
        public Vector3[] update(float updateDeltaTime)
        {
            
            float deltaTime = (float) (updateDeltaTime * engineTimeCoefficient);
            GameEvents.current.virtualTimeIncrement(deltaTime);
            
            // Upadating the buffers 
            shader.SetFloat("deltaTime", deltaTime);
            planetBuffer.SetData(_planetDatas);
            shader.SetBuffer(kernel, "planetsData", planetBuffer);
            shader.SetBuffer(kernel, "Result", resultBuffer);

            // Running the compute 
            int threadGroups = (int) ((planetsAmount + (threadGroupSize - 1)) / threadGroupSize);
            shader.Dispatch(kernel, threadGroups, 1, 1);

            resultBuffer.GetData(output);
            planetBuffer.GetData(_planetDatas);
            
            return output;
        }
        
        public PlanetData[] planetDatas()
        {
            return _planetDatas;
        }

        public void dispose()
        {
            resultBuffer?.Dispose();
            planetBuffer?.Dispose();
        }
    }
    
}