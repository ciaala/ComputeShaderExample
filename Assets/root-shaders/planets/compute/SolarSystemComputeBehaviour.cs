using System;
using UnityEngine;
using Console = common.Console;

namespace planets.compute
{
    public class SolarSystemComputeBehaviour : MonoBehaviour
    {
        private readonly SolarSystemModel model;

        private SolarSystemCompute compute;
        private Transform[] instances;
        public GameObject planetPrefab;
        public ComputeShader shader;
        public GameObject sunGameObject;
        private GameObject[] planetGameObjects;
        private float previousTime = 0;

        public SolarSystemComputeBehaviour()
        {
            model = new SolarSystemModel();
        }

        private void Awake()
        {
            compute = new SolarSystemCompute(model, GameEvents.current, shader);
            compute.initSolarSystem();
        }

        private void Start()
        {
            //spheres we use for visualisation
            createPlanetInstances();
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            Vector3[] output = compute.update(Time.fixedDeltaTime);
            updatePlanetGameObjects(output, TimeManager.current.virtualTimeString);

/*
            if (tails.Count == tails.Capacity)
            {
                tails.RemoveAt(tails.Count - 1);
            }

            tails.Insert(0, output);

            tails[0] = output;
            */
        }

        private int planetsAmount => compute.planetDatas().Length;


        private void createPlanetInstances()
        {
            instances = new Transform[compute.planetDatas().Length];
            planetGameObjects = new GameObject[planetsAmount];
            Debug.LogWarning("planets " + planetsAmount);
            {
                float scaleFactor = (float) Math.Log(model.sun.diameter);
                sunGameObject.transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);
            }
            for (int i = 0;
                i < planetsAmount;
                i++)
            {
                planetGameObjects[i] = Instantiate(planetPrefab, transform);
                GameObject planetGameObject = planetGameObjects[i];
                planetGameObject.name = model.planets[i].name;
                float scaleFactor = (float) Math.Log(model.planets[i].diameter);
                planetGameObject.transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);
                instances[i] = planetGameObject.transform;
            }
        }

        void OnDestroy()
        {
            compute.dispose();
        }

        private void updatePlanetGameObjects(Vector3[] output, String currentVirtualTime)
        {
            bool shouldLog = (int) Time.time % 3 == 0 && (int) Time.time != (int) previousTime;
            if (shouldLog)
            {
                Debug.Log("-----------------------");
                Debug.Log("virtualTime: " + currentVirtualTime);
            }

            PlanetData[] planetDatas = compute.planetDatas();
            for (int i = 0; i < instances.Length; i++)
            {
                Transform planetTransform = instances[i];
                Vector3 newLocalPosition = output[i];
                if (shouldLog && i == 2)
                {
                    previousTime = Time.time;
                    // float d = _planetDatas[i].orbitalPeriod * deltaTime + _planetDatas[i].microAngularPosition;
                    Debug.Log(model.planets[i].name + ": " + output[i] + " ### " + planetDatas[i]);
                }

                if (i == 2 && newLocalPosition.y > 0 && planetTransform.localPosition.y < 0)
                {
                    Console.log("Time has flown " + TimeManager.current.virtualTimeString);
                }

                planetTransform.localPosition = newLocalPosition;
            }
        }
    }
}