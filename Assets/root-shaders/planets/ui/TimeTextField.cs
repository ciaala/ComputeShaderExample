﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using planets;
using UnityEngine;
using UnityEngine.UI;

public class TimeTextField : MonoBehaviour
{
    private TimeManager timeManager;
    public Text textField;

    public Text portugal;

    //private double totalVirtualTime;

    // Start is called before the first frame update
    void Start()
    {
        timeManager = gameObject.AddComponent<TimeManager>();
        GameEvents.current.onVirtualTimeChange += onVirtualTimeChange;
        GameEvents.current.onVirtualTimeUpdate += onVirtualTimeUpdate;
    }

    private void onVirtualTimeUpdate(String value)
    {
        portugal.text = value;
    }

    private void onVirtualTimeChange(String value)
    {
        textField.text = value;
    }
}