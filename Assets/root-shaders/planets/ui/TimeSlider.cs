﻿using System;
using System.Collections;
using System.Collections.Generic;
using planets;
using UnityEngine;
using UnityEngine.UI;

public class TimeSlider : MonoBehaviour
{ 
    public float timeSlide = 1;
    // Start is called before the first frame update
    void Start() {
    }

    public Slider slider;
   
    // Update is called once per frame
    void Update()
    {
        if (Math.Abs(slider.value - timeSlide) > 0.001)
        {
            GameEvents.current.timeSliderChange(slider.value);
            //    sendEvent("setProperty", "zoom", slider.value);
            timeSlide = slider.value;
        }
    }
}