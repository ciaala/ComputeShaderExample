﻿using System;
using UnityEngine;

namespace planets
{
    public class GameEvents : MonoBehaviour
    {
        public static GameEvents current;


        public void Awake()
        {
            current = this;
        }

        public void Start()
        { throw new NotImplementedException();
        }

        public event Action<float> onTimeSliderChange;

        public event Action<float> onVirtualTimeIncrement;

        public event Action<String> onVirtualTimeChange;
        public event Action<String> onVirtualTimeUpdate;

        public void timeSliderChange(float timeCoefficient)
        {
            onTimeSliderChange?.Invoke(timeCoefficient);
        }

        public void virtualTimeChange(string value)
        {
            onVirtualTimeChange?.Invoke(value);
        }

        public void virtualTimeUpdate(string value)
        {
            onVirtualTimeUpdate?.Invoke(value);
        }

        public void virtualTimeIncrement(float value)
        {
            onVirtualTimeIncrement?.Invoke(value);
        }
    }
}