﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.PlayerLoop;
using UnityEngine.UIElements;

public class UIController : MonoBehaviour
{
    private float zoom = 1;

    public Slider slider;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (slider.value != zoom)
        {
            sendEvent("setProperty", "zoom", slider.value);
            zoom = slider.value;
        }
    }

    private void sendEvent(string eventType, string eventName, float newValue)
    {
        
    }
}