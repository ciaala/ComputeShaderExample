﻿using System.Collections;
using NUnit.Framework;
using UnityEngine.TestTools;
using Assert = UnityEngine.Assertions.Assert;

namespace planets.model
{
    public class SolarSystemTest
    {
        // A Test behaves as an ordinary method
        [Test]
        public void test_Constructor_SolarSystemModel_IsInitialized()
        {
            // Use the Assert class to test conditions
            SolarSystemModel solarSystem = new SolarSystemModel();
            PlanetModel[] planets = solarSystem.getPlanets();
            Assert.IsTrue(planets.Length > 0);
        }

        
        
        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator SolarSystemTestWithEnumeratorPasses()
        {
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }
    }
}
