﻿using System.Collections;
using System.Collections.Generic;
using common;
using NUnit.Framework;
using UnityEditor.iOS;
using UnityEngine;
using UnityEngine.TestTools;

namespace planets.compute
{
    public class SolarSystemComputeTest
    {
        private ComputeShader computeShader;

        [SetUp]
        public void Setup()
        {
            string computeShaderResource = "planets";
            computeShader = ComputeShaderFinder.find(computeShaderResource);
        }

        private GameEvents getGameEvents()
        {
            GameEvents gameEvents = MonoBehaviour.FindObjectOfType<GameEvents>();
            gameEvents.Awake();
            gameEvents.Start();
            return gameEvents;
        } 

        // A Test behaves as an ordinary method
        [Test]
        public void test_initialization_create_computes_data()
        {
            SolarSystemCompute compute =
                new SolarSystemCompute(new SolarSystemModel(), GameEvents.current, computeShader);
            compute.initSolarSystem();
            Assert.IsNotNull(compute.planetDatas());
            Assert.IsNotEmpty(compute.planetDatas());
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator ComputeTestWithEnumeratorPasses()
        {
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }
    }
}